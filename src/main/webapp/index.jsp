<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Create New User</title>
</head>
<body>
<h1><%= "Please create a New User account" %>
</h1>
<br/>
<p>To create a new user account click <a href="${pageContext.request.contextPath}/login.html">here</a></p>
</body>
</html>